### This part is something I would modularize if it werent for the assessment.
### This part has a lot of duplicate code and I personally would build a "Microservice Module" that handles API + Lambda Connections and normally far more dynamic

resource "aws_api_gateway_rest_api" "rest_interface" {
  name = "Demo"
}

resource "aws_api_gateway_request_validator" "request_validator" {
  name                        = "Validate body, query string parameters, and headers"
  rest_api_id                 = aws_api_gateway_rest_api.rest_interface.id
  validate_request_body       = true
  validate_request_parameters = true
}


resource "aws_api_gateway_resource" "split_path" {
  rest_api_id = aws_api_gateway_rest_api.rest_interface.id
  parent_id   = aws_api_gateway_rest_api.rest_interface.root_resource_id
  path_part   = "split"
}

resource "aws_api_gateway_resource" "compare_path" {
  rest_api_id = aws_api_gateway_rest_api.rest_interface.id
  parent_id   = aws_api_gateway_rest_api.rest_interface.root_resource_id
  path_part   = "compare"
}

resource "aws_api_gateway_resource" "sort_path" {
  rest_api_id = aws_api_gateway_rest_api.rest_interface.id
  parent_id   = aws_api_gateway_rest_api.rest_interface.root_resource_id
  path_part   = "sort"
}

resource "aws_api_gateway_method" "split_post_method" {
  rest_api_id          = aws_api_gateway_rest_api.rest_interface.id
  resource_id          = aws_api_gateway_resource.split_path.id
  http_method          = "POST"
  authorization        = "NONE" #Definitely not best practise but for the demo usability enough
  request_parameters   = { "method.request.querystring.splitSize" = false }
  request_models       = { "application/json" = aws_api_gateway_model.array_model.name }
  request_validator_id = aws_api_gateway_request_validator.request_validator.id
}

resource "aws_api_gateway_method" "compare_post_method" {
  rest_api_id          = aws_api_gateway_rest_api.rest_interface.id
  resource_id          = aws_api_gateway_resource.compare_path.id
  http_method          = "POST"
  authorization        = "NONE" #Definitely not best practise but for the demo usability enough
  request_models       = { "application/json" = aws_api_gateway_model.array_model.name }
  request_validator_id = aws_api_gateway_request_validator.request_validator.id
}

resource "aws_api_gateway_method" "sort_post_method" {
  rest_api_id          = aws_api_gateway_rest_api.rest_interface.id
  resource_id          = aws_api_gateway_resource.sort_path.id
  http_method          = "POST"
  authorization        = "NONE" #Definitely not best practise but for the demo usability enough
  request_parameters   = { "method.request.querystring.algorithm" = false }
  request_models       = { "application/json" = aws_api_gateway_model.array_model.name }
  request_validator_id = aws_api_gateway_request_validator.request_validator.id
}

resource "aws_api_gateway_integration" "sort_integration_rest" {
  rest_api_id             = aws_api_gateway_rest_api.rest_interface.id
  resource_id             = aws_api_gateway_resource.sort_path.id
  http_method             = "POST"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.sfn_lambda.invoke_arn
}

resource "aws_api_gateway_integration" "split_integration_rest" {
  rest_api_id             = aws_api_gateway_rest_api.rest_interface.id
  resource_id             = aws_api_gateway_resource.split_path.id
  http_method             = "POST"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.split_lambda.invoke_arn
}

resource "aws_api_gateway_integration" "compare_integration_rest" {
  rest_api_id             = aws_api_gateway_rest_api.rest_interface.id
  resource_id             = aws_api_gateway_resource.compare_path.id
  http_method             = "POST"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.compare_lambda.invoke_arn
}

resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [
    aws_api_gateway_integration.compare_integration_rest,
    aws_api_gateway_integration.split_integration_rest,
    aws_api_gateway_integration.sort_integration_rest,
  ]
  rest_api_id = aws_api_gateway_rest_api.rest_interface.id
}

resource "aws_api_gateway_stage" "stage" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.rest_interface.id
  stage_name    = "default"
}

resource "aws_lambda_permission" "compare_api_lambda_invoke_permissions" {
  statement_id  = "AllowExecutionFromAPIGatewayPOST"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.compare_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.rest_interface.id}/*/POST${aws_api_gateway_resource.compare_path.path}"
}

resource "aws_lambda_permission" "sort_api_lambda_invoke_permissions" {
  statement_id  = "AllowExecutionFromAPIGatewayPOST"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sfn_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.rest_interface.id}/*/POST${aws_api_gateway_resource.sort_path.path}"
}

resource "aws_lambda_permission" "split_api_lambda_invoke_permissions" {
  statement_id  = "AllowExecutionFromAPIGatewayPOST"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.split_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.rest_interface.id}/*/POST${aws_api_gateway_resource.split_path.path}"
}

resource "aws_api_gateway_model" "array_model" {
  rest_api_id  = aws_api_gateway_rest_api.rest_interface.id
  name         = "array"
  description  = "a JSON schema to specify array as allowed input"
  content_type = "application/json"

  schema = jsonencode({
    type     = "object",
    required = ["array"],
    properties = {
      array = {
        type = "array"
      }
    }

  })
}
