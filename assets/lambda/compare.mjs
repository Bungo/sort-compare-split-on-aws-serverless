export const handler = function (event, context, callback) {
    const eventbody = JSON.parse(event.body);
    var res = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        }
    };
    if (eventbody.array.length < 2) {
        callback(null, { "array": [] });
    }

    const missings = [];
    missings.push(eventbody.array[0].filter((o) => eventbody.array[eventbody.array.length - 1].indexOf(o) === -1));
    for (let i = 1; i < eventbody.array.length; i++) {
        missings.push(eventbody.array[i].filter((o) => eventbody.array[i - 1].indexOf(o) === -1));
    }
    let uniqueMissings = new Set(missings.flat());
    res.body = JSON.stringify({ "array": Array.from(uniqueMissings) });
    callback(null, res);
}