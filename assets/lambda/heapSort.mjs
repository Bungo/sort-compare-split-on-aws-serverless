export const handler = function (event, context, callback) {
  callback(null, { "array": heapSort(event.array) });
}

function heapify(elems, len, i) {
  const left = 2 * i + 1;
  const right = 2 * i + 2;
  let max = i;
  if (left < len && concatElemNumbers(elems[left]) > concatElemNumbers(elems[max])) max = left;
  if (right < len && concatElemNumbers(elems[right]) > concatElemNumbers(elems[max])) max = right;
  if (max != i) {
    [elems[max], elems[i]] = [elems[i], elems[max]];
    heapify(elems, max);
  }
}

function heapSort(elems) {
  let len = elems.length
  for (let i = Math.floor(elems.length / 2 - 1); i >= 0; i--) {
    heapify(elems, len, i);
  }
  for (let i = elems.length - 1; i > 0; i--) {
    [elems[0], elems[i]] = [elems[i], elems[0]];
    len--;
    heapify(elems, len, 0);
  }
  return elems
}

function concatElemNumbers(elem) {
  var invoice = "0." + elem.invoice.number
  return elem.customer_number + 1 - invoice //We do 1 - invoice, because if customer_number is equal on both elements, we want the invoice descending
}