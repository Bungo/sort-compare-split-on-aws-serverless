export const handler = function (event, context, callback) {
    callback(null, { "array": quickSort(event.array) });
}


function quickSort(elems) {
    if (elems.length < 2) return elems;

    let pivot = elems[elems.length - 1];
    const left = [];
    const right = [];

    for (let elem of elems.slice(0, elems.length - 1)) {
        if (concatElemNumbers(elem) > concatElemNumbers(pivot)) {
            right.push(elem)
        } else {
            left.push(elem)
        }
    }

    return [...quickSort(left), pivot, ...quickSort(right)]
}

function concatElemNumbers(elem) {
    var invoice = "0." + elem.invoice.number
    return elem.customer_number + 1 - invoice //We do 1 - invoice, because if customer_number is equal on both elements, we want the invoice descending
}