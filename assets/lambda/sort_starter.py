#I decided to go for python here, because simplicity is key
#and if you don't believe me, just check this example to achieve the same: https://stackoverflow.com/questions/66915029/how-to-call-a-step-funtion-from-node-js-lambda-function
#It is much more (and less readable) code for the same simple result of passing the input to the steps

import boto3
import json
import os

client = boto3.client('stepfunctions')

def lambda_handler(event, context):
    input = json.loads(event['body'])
    input['algorithm'] = event['queryStringParameters']['algorithm'] if event['queryStringParameters'] else ""
    
    response = client.start_sync_execution(
        stateMachineArn=os.environ['SFN'],
        input=json.dumps(input)
    )
    print(response)
    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": response["output"]
    }
