export const handler = function (event, context, callback) {
    const eventbody = JSON.parse(event.body);
    var res = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        }
    };
    let splitSize = event.queryStringParameters == null ? process.env.ARRAY_SIZE_DEFAULT : event.queryStringParameters.splitSize;
    const parent = [];
    while (eventbody.array.length > splitSize) {
        let item = eventbody.array.splice(0, splitSize);
        parent.push(item);
    }
    parent.push(eventbody.array);

    res.body = JSON.stringify({ "array": parent });
    callback(null, res);
}