export const handler = function (event, context, callback) {
    for (let i = 1; i < event.array.length; i++) {
        let elem = event.array[i];
        let pos = i;
        while (pos > 0 && (event.array[pos - 1].customer_number > elem.customer_number ||
            (event.array[pos - 1].customer_number == elem.customer_number && event.array[pos - 1].invoice.number < elem.invoice.number))) {
            event.array[pos] = event.array[pos - 1];
            pos--;
        }
        event.array[pos] = elem;
    }
    callback(null, { "array": event.array });
}