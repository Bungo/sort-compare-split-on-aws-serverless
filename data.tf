data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
data "archive_file" "js_lambda_files" {
  for_each    = fileset("", "assets/lambda/*.mjs")
  type        = "zip"
  source_file = each.value
  output_path = "${each.value}.zip"
}
data "archive_file" "py_lambda_file" {
  type        = "zip"
  source_file = "assets/lambda/sort_starter.py"
  output_path = "assets/lambda/sort_starter.zip"
}
