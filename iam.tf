resource "aws_iam_role" "sfn_lambda_role" {
  #Since it is an assessment, I use managed roles with a lot of privileges. Normally I would write my own policies for least privilege, but this would be overkill
  name                = "sfn-lambda-role"
  managed_policy_arns = ["arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess", "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"]
  assume_role_policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Action : "sts:AssumeRole",
        Principal : {
          Service : "lambda.amazonaws.com"
        },
        Effect : "Allow",
        Sid : ""
      }
    ]
  })

}

resource "aws_iam_role" "basic_lambda_role" {
  #Since it is an assessment, I use managed roles with a lot of privileges. Normally I would write my own policies for least privilege, but this would be overkill
  name                = "basic-role"
  managed_policy_arns = ["arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"]
  assume_role_policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Action : "sts:AssumeRole",
        Principal : {
          Service : "lambda.amazonaws.com"
        },
        Effect : "Allow",
        Sid : ""
      }
    ]
  })

}

resource "aws_iam_role" "sfn_role" {
  #Since it is an assessment, I use managed roles with a lot of privileges. Normally I would write my own policies for least privilege, but this would be overkill
  name                = "sfn-role"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSLambdaRole", "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"]
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "states.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })

}
