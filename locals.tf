locals {
  sfn_choices_algos = [for _, v in aws_lambda_function.sort_lambdas :
    { "Variable" : "$.algorithm",
      "StringEquals" : split("Sort", v.function_name)[0],
    "Next" : v.function_name }
  ]

  sfn_algo_states = { for _, v in aws_lambda_function.sort_lambdas : v.function_name =>
    { "Type" : "Task",
      "Resource" : "arn:aws:states:::lambda:invoke",
      "OutputPath" : "$.Payload",
      "Parameters" : {
        "Payload.$" : "$",
        "FunctionName" : v.arn
      },
      "End" : true
    }
  }
}
