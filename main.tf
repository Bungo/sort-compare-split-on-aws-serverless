resource "aws_lambda_function" "sfn_lambda" {
  filename      = data.archive_file.py_lambda_file.output_path
  function_name = "sfn-starter"
  role          = aws_iam_role.sfn_lambda_role.arn
  handler       = "sort_starter.lambda_handler"
  timeout       = 60


  runtime = "python3.12"

  environment {
    variables = {
      SFN = aws_sfn_state_machine.sfn_state_machine.arn
    }
  }
}

resource "aws_lambda_function" "sort_lambdas" {
  for_each      = { for k, v in data.archive_file.js_lambda_files : k => v if length(regexall("Sort", k)) > 0 }
  filename      = each.value.output_path
  function_name = split(".", reverse(split("/", each.value.output_path))[0])[0]
  role          = aws_iam_role.basic_lambda_role.arn
  timeout       = 300 // Just to be sure that we can even handle bigger sorting
  handler       = "${split(".", reverse(split("/", each.value.output_path))[0])[0]}.handler"
  runtime       = "nodejs20.x"
}

resource "aws_lambda_function" "split_lambda" {
  filename      = "assets/lambda/split.mjs.zip"
  function_name = "split-function"
  role          = aws_iam_role.basic_lambda_role.arn
  timeout       = 300 // Just to be sure that we can even handle bigger splits
  handler       = "split.handler"
  runtime       = "nodejs20.x"

  environment {
    variables = {
      ARRAY_SIZE_DEFAULT = var.default_array_split_size
    }
  }
}

resource "aws_lambda_function" "compare_lambda" {
  filename      = "assets/lambda/compare.mjs.zip"
  function_name = "compare-function"
  role          = aws_iam_role.basic_lambda_role.arn
  timeout       = 300 // Just to be sure that we can even handle bigger compares
  handler       = "compare.handler"
  runtime       = "nodejs20.x"
}

resource "aws_cloudwatch_log_group" "sfn_logs" {
  name = "sfn-logs"
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "sort-sfn"
  role_arn = aws_iam_role.sfn_role.arn
  type     = "EXPRESS"

  logging_configuration {
    log_destination        = "${aws_cloudwatch_log_group.sfn_logs.arn}:*"
    include_execution_data = true
    level                  = "ALL"
  }

  definition = jsonencode({
    "Comment" : "This Step-Function processes requests for sorting",
    "StartAt" : "PreProcessing",
    "States" : merge({
      "PreProcessing" : {
        "Type" : "Pass",
        "Parameters" : {
          "arrayLen.$" : "States.ArrayLength($.array)",
          "array.$" : "$.array",
          "algorithm.$" : "$.algorithm"
        },
        "Next" : "CheckLength"
      }
      "CheckLength" : {
        "Type" : "Choice",
        "Choices" : [
          {
            "Variable" : "$.arrayLen",
            "NumericLessThan" : 2,
            "Next" : "EndPass"
          },
        ]
        "Default" : "SortAlgorithm",
      },
      "SortAlgorithm" : {
        "Type" : "Choice",
        "Choices" : local.sfn_choices_algos
        "Default" : "${var.default_sort_algorithm}Sort",
      },
      "EndPass" : {
        "Type" : "Pass",
        "Parameters" : {
          "array.$" : "$.array"
        },
        "End" : true
      }
    }, local.sfn_algo_states)
  })
}
