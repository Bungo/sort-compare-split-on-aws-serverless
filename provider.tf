provider "aws" {
  default_tags {
    tags = {
      Application = "coding-challenges"
    }
  }
}
