variable "default_array_split_size" {
  type        = number
  description = "The default value that the array splitter uses if no param splitSize is defined in api request"
  default     = 3
}

variable "default_sort_algorithm" {
  type        = string
  description = "The default value that the array sorter uses if no param algorithm is defined in api request"
  default     = "heap"
}
